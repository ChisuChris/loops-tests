//Creates an array.
const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];
console.log(arr);

//Prints items which start letter "t".
const startsWithT = arr.filter((vihannes) => vihannes.startsWith("t"));
console.log(startsWithT);

//Check if array contains specific items.
const includeCheck = arr.includes("mandariini" && "tomaatti"); 
console.log(includeCheck);

//Sorts array alphabetically.
const alphSort = arr.sort();
console.log(alphSort);

//Removes last item.
const popArr = arr.pop();
console.log(arr);

//Adds new item to the end of array.
const sipuliArr = arr.push("sipuli");
console.log(arr);

//Reverses the whole array.
const arrRev = arr.reverse();
console.log(arrRev)
